from flask import Flask, render_template, request,jsonify
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///student.db"
db = SQLAlchemy(app)

class Student(db.Model):
    sno = db.Column(db.Integer ,primary_key = True)
    name = db.Column(db.String(200) ,nullable= False)
    rollno = db.Column(db.Integer ,nullable= False)
    age = db.Column(db.Integer ,nullable= False)
    gender = db.Column(db.String(500) ,nullable= False)

    def __init__(self,name,rollno,age,gender):
        self.name = name
        self.rollno = rollno
        self.age = age
        self.gender = gender

@app.route('/', methods=['POST'])
def insert():
    if request.method == 'POST':
        name = request.form['name']
        rollno = request.form['rollno']
        age = request.form['age']
        gender = request.form['gender']
        student = Student(name= name, rollno = rollno,age= age, gender = gender)
        db.session.add(student)
        db.session.commit()
    student = Student.query.all()
    print(student)
    return jsonify({"name":name,"rollno":rollno,"age":age,"gender":gender})

@app.route('/delete/<int:sno>')
def delete(sno):
    student = Student.query.filter_by(sno=sno).first()
    db.session.delete(student)
    db.session.commit()
    return jsonify({"Result":"Data deleted"})

@app.route('/update/<int:sno>', methods = ['POST'])
def update(sno):
    if request.method == 'POST':
        name = request.form['name']
        rollno = request.form['rollno']
        age = request.form['age']
        gender = request.form['gender']
        student= Student.query.filter_by(sno=sno).first()
        student.name = name
        student.rollno = rollno
        student.age = age
        student.gender = gender
        db.session.add(student)
        db.session.commit()
        return jsonify({"Result":"Data Updated"})


@app.route('/search', methods = ['GET'])
def allstudent():
    student = Student.query.all()
    return jsonify({"Result":student})




if __name__ == "__main__":
    # db.create_all()
    app.run(debug = True)
