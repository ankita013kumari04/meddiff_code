def isPalindrome(word):
  word = word.lower()
  rev = word[::-1]
  if(word==rev):
      return "is palindrome"
  else:
      return "not palindrome"

word = input()
print(isPalindrome(word))