import openpyxl
import pandas as pd
import os

#loading Excel sheet
def load_workbook(wb_path):
    if os.path.exists(wb_path):
        return openpyxl.load_workbook(wb_path)
    else:
        return "file not found..."

wb_path = "student.xlsx"
wb = load_workbook(wb_path)
sheet = wb["Sheet1"]
sheet_obj = wb.active
max_row = sheet_obj.max_row
max_column = sheet_obj.max_column

#create student
def Create_Student():
    new_student = input("\n Enter student details Name, Roll No, Age and Gender").split(' ')
    sheet.append(new_student)
    wb.save(wb_path)
    print("Student Details Added succesfully")

def View_All_Student():
    student_list = pd.read_excel(wb_path)
    print(student_list)

def search(name):
    for i in range(1,max_row+1):
        if sheet.cell(row=i,column = 1).value == name:
            print("Student found")
            return i

def Display_student(row):
    for i in range(1,max_column +1):
        cell_obj = sheet_obj.cell(row = row,column = i)
        print(cell_obj.value)

def Update_student(row):
    x= input("\n Enter student details Name, Roll No, Age and Gender").split(' ')
    for col_index,value in enumerate(x,start=1):
        sheet.cell(row= row,column = col_index,value = value)
    wb.save(wb_path)
    print("\n Student details updated successfully")

def Delete_student(row):
    sheet.delete_rows(row)
    wb.save(wb_path)
    print('Student deleted successfully')

while True:
    print("\n Student management")
    print("\n1. Create student")
    print("\n2. View all student")
    print("\n3. Search student")
    print("\n4. Update student")
    print("\n5. Delete student")
    ch = input("\n enter the option")
    if ch == '1':
        Create_Student()
    if ch == '2':
        View_All_Student()
    if ch == '3':
        x= input("Enter the student name")
        row = search(x)
        Display_student(row)    
    if ch == '4':
        x= input("Enter the student name to update")
        row = search(x)
        Update_student(row)
    if ch == '5':
        x= input("Enter the student name to delete")
        row = search(x)
        print(row)
        Delete_student(row)
    else:
        break
